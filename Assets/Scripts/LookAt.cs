using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;

public class LookAt : MonoBehaviour
{
    public Transform eyePosition;

    public float visionDistance = 7f;       //How far should the character see to start turning head at seen objects.            
    public Vector3 fieldOfView = new Vector3(12.2f, 4f, 0.5f);
    public LayerMask seeObjectsOnLayer;
    public Rig headAimRig;
    public Transform rigTarget;    
    public bool debug = false;
    
    private bool m_HitDetect;
    private RaycastHit m_Hit;
    private LookingState currentState;
    private Vector3 lookPosition;
    private float headTurnDuration = 2.5f;  //time in seconds for head to turn when changing from one POI to another.

    enum LookingState
    {
        Nothing,
        Looking,
        TurningAway
    };

    void Start()
    {
        
    }


    void FixedUpdate()
    {
        LookingTest();
    }

    void Update()
    {
       if (currentState == LookingState.Looking)
        {
            if (IsTargetBehindTest())
            {
                LookAway();
            }
        }
    }

    private void LookingTest()
    {

        /*
        RaycastHit[] BoxCastAll(Vector3 center, Vector3 halfExtents, Vector3 direction, Quaternion orientation = Quaternion.identity,
            float maxDistance = Mathf.Infinity, int layerMask = DefaultRaycastLayers,
            QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal);
        */
        
        //Test to see if there is an object of interest using a BoxCast
        m_HitDetect = Physics.BoxCast(eyePosition.position, fieldOfView, transform.forward, out m_Hit, transform.rotation, visionDistance, seeObjectsOnLayer);
        
        if (m_HitDetect)
        {            
            //use the hit objects collider center position as where to look. 
            lookPosition = m_Hit.collider.transform.position;
            StartCoroutine("TurnHeadAtObject", lookPosition);
        }


    }
    
    IEnumerator TurnHeadAtObject(Vector3 targetPosition)
    {

        // exit coroutine early if already looking at the same position
        if (rigTarget.position == targetPosition && currentState == LookingState.Looking) yield break;
        

        //looking at something else;
        if (rigTarget.position != targetPosition)
        {
            // If currently looking at something else.  Smooth out the target position so the head doesn't jerk
            if (headAimRig.weight != 0f)
            {
                //smooth change current looking position
                float elapsedTime = 0f;                

                while (elapsedTime < headTurnDuration && lookPosition == targetPosition)  //race condition.  method could be called again while smoothing is still occuring for another POI
                                                         //exit smoothing early if character has started looking at a new target while in this loop.
                {                    
                    rigTarget.position = Vector3.Lerp(rigTarget.position, targetPosition, elapsedTime / headTurnDuration);
                    elapsedTime += Time.deltaTime;

                    yield return null;
                }
            }   
        }
        
        currentState = LookingState.Looking;
        //before can switch between different look at targets need to smooth head turn back to zero
        StartCoroutine("SmoothRig", new float[] { headAimRig.weight, 1f });

    }

    bool IsTargetBehindTest()
    {
        if (rigTarget)
        {
            Vector3 forward = transform.TransformDirection(Vector3.forward);
            Vector3 toOther = rigTarget.position - transform.position;

            if (Vector3.Dot(forward, toOther) < 0)
            {
                return true;
            }
        }

        return false;
    }

    private void LookAway()
    {        
        currentState = LookingState.TurningAway;
        StartCoroutine("SmoothRig", new float[] { headAimRig.weight, 0 });        
    }

    IEnumerator SmoothRig(float[] startEnd)
    {
        if (startEnd.Length != 2) yield return null;

        LookingState startingState = currentState;
        float start; float end;
        start = startEnd[0];
        end = startEnd[1];

        float elapsedTime = 0;
        float waitTime = 0.6f;

       
        while ((elapsedTime < waitTime)
            && startingState == currentState)  //breakout early if character has turned away or has started looking at another object
        {            
            headAimRig.weight = Mathf.Lerp(start, end, (elapsedTime / waitTime));
            elapsedTime += Time.deltaTime;

            yield return null;
        }
    }


    public Vector3 CenterOfRaycastHits(RaycastHit[] hits)
    {
        Vector3 sum = Vector3.zero;
        if (hits == null || hits.Length == 0)
        {
            return sum;
        }

        foreach (RaycastHit hit in hits)
        {
            sum += hit.collider.transform.position;
        }
        return sum / hits.Length;
    }

    //Draw the BoxCast as a gizmo to show where it currently is testing. Click the Gizmos button to see this
    void OnDrawGizmos()
    {

        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(rigTarget.position, 0.28f);


        if (debug)
        {
            Gizmos.color = Color.red;

            //Check if there has been a hit yet
            if (m_HitDetect)
            {
                //Draw a Ray forward from GameObject toward the hit
                Gizmos.DrawRay(eyePosition.position, transform.forward * m_Hit.distance);
                //Draw a cube that extends to where the hit exists
                Gizmos.DrawWireCube(eyePosition.position + transform.forward * m_Hit.distance, fieldOfView);
            }
            //If there hasn't been a hit yet, draw the ray at the maximum distance
            else
            {
                //Draw a Ray forward from GameObject toward the maximum distance
                Gizmos.DrawRay(eyePosition.position, transform.forward * visionDistance);
                //Draw a cube at the maximum distance
                Gizmos.DrawWireCube(eyePosition.position + transform.forward * visionDistance, fieldOfView);
            }
        }
    }
}
