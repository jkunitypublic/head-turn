using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class HapticsTest : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {        
        InvokeRepeating("PlayHapticsOneShot", 5f, 4f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void PlayHapticsOneShot()
    {
        Debug.Log("PlayHaptics");
        StartCoroutine(PlayHaptics(.8f));
    }

    IEnumerator PlayHaptics(float seconds)
    {
        Gamepad.current.SetMotorSpeeds(0.25f, 0.25f);        
        yield return new WaitForSeconds(seconds);
        InputSystem.ResetHaptics();
    }
}
